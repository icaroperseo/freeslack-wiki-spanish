[FreeSlack](http://freeslack.net/dokuwiki/doku.php?id=start) wiki translation from English to Spanish language.

Except where otherwise noted, content on this repository is licensed under the following license: [GNU Free Documentation License 1.3](https://www.gnu.org/copyleft/fdl.html) or any later version.
