====== Slackware 14.0 ======

El siguiente archivo contiene las licencias de los paquetes de Slackware 14.0: [[http://www.freeslack.net/slackware-14.0-licenses.csv|slackware-14.0-licenses.csv]]. El archivo CSV se encuentra codificado en formato UTF-8, empleando tabuladores como separadores y con ''"'' como delimitadores.

La siguiente tabla lista todos los paquetes contenidos en el DVD de Slackware 64 que incorporan software no libre. Observe que los comentarios no sustituyen a sus respectivas licencias. En general, la leyenda «no comercial» pude limitar su uso, o distribución, o bien, ambos casos. Consulte las respectivas licencias para obtener más detalles al respecto.

Tenga en cuenta que la metodología ha cambiado desde la versión 14.0 por lo que esta sección luce mucho más breve de lo que debería ser. La tabla mostrada para la versión [[slackware_14.1|14.1]] luce mucho más larga, sin embargo, ningún paquete de software no libre fue agregado entre ambas versiones.

^ Nombre ^ Comentario ^ Referencia ^
| extra/google-chrome/google-chrome-pam-solibs-1.1.3-x86_64-1.txz | Dado que Chrome es software no libre no se observa algún punto para este paquete.                         |                        |
| slackware64/a/getty-ps-2.1.0b-x86_64-1.txz                      | Sólo para uso no comercial.                                                                             | main.c                 |
| slackware64/a/kernel-firmware-20120804git-noarch-1.txz          | Incluye fragmentos de software no libre.                                                                |                        |
| slackware64/a/lha-114i-x86_64-1.txz                             | Licencia demasiado ambigua de acuerdo a la <abbr>FSF [Free Software Foundation]</abbr>.                 |                        |
| slackware64/a/unarj-265-x86_64-1.txz                            | No puede utilizar el código en archivos ARJ con soporte para codificar/descodificar.                    | unarj.c                |
| slackware64/ap/amp-0.7.6-x86_64-1.txz                           | Sólo para uso no comercial.                                                                             | README                 |
| slackware64/n/bluez-firmware-1.2-x86_64-1.txz                   | Todos los derechos reservados, no se incluye el código fuente.                                          | broadcom/BCM-LEGAL.txt |
| slackware64/n/ipw2100-fw-1.3-fw-1.txz                           | Sin código fuente, no se permiten modificaciones y más.                                                 | LICENSE                |
| slackware64/n/ipw2200-fw-3.1-fw-1.txz                           | Sin código fuente, no se permiten modificaciones y más.                                                 | LICENSE.ipw2200-fw     |
| slackware64/n/trn-3.6-x86_64-2.txz                              | Sólo para uso no comercial.                                                                             | EXTERN.h               |
| slackware64/n/zd1211-firmware-1.4-fw-1.txz                      | No incluye el código fuente.                                                                            | WS11UPh.h              |
| slackware64/xap/xfractint-20.04p11-x86_64-1.txz                 | Sólo para uso no comercial.                                                                             | fractsrc.txt           |
| slackware64/xap/xgames-0.3-x86_64-4.txz                         | xminesweep es sólo para uso no comercial.                                                               | xminesweep.c           |
| slackware64/xap/xv-3.10a-x86_64-6.txz                           | No se permite la distribución de copias modificadas, sólo para uso no comercial, sólo para uso personal. | copyright.h            |

Además, existen algunos SlackBuilds que permiten la instalación de software no libre:

<code>
extra/source/flashplayer-plugin/flashplayer-plugin.SlackBuild
extra/google-chrome/google-chrome.SlackBuild
extra/source/java/java.SlackBuild
</code>

===== Metodología =====

La metodología aquí empleada fue exactamente la misma que la [[slackware_14.1#metodologia|utilizada para Slackware 14.1]] salvo sutiles diferencias.

# vi:syntax=dokuwiki tw=0 wrapmargin=0 spell spelllang=es
